package com.example.RiddleMeThis.service.serviceImplTest;

import com.example.RiddleMeThis.dto.PostOptionDTO;
import com.example.RiddleMeThis.dto.PostParticipantResponseDTO;
import com.example.RiddleMeThis.dto.AnswerDTO;
import com.example.RiddleMeThis.entities.*;
import com.example.RiddleMeThis.repository.*;
import com.example.RiddleMeThis.service.serviceImpl.ParticipantResponseServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ParticipantResponseServiceImplTest {

    @Autowired
    private ParticipantResponseServiceImpl participantResponseServiceImpl;
    @Autowired
    private ParticipantResponseRepository participantResponseRepository;
    @Autowired
    private UserRepository userRepository;

    User demoUser = new User();
    Quiz demoQuiz = new Quiz();
    Question demoQuestion01 = new Question();
    Option demoOption01 = new Option();

    @Before
    public void before()  {
        System.out.println("Before every test");

        demoUser = new User(new Date(), new Date(), "Batman", "darknight@gotham.com", "batcave");
        demoQuiz = new Quiz(new Date(), new Date(), "demo quiz", demoUser);
        demoQuiz.setUuid();

        demoQuestion01 = new Question(demoQuiz, "What is the colour of the sky", QuestionType.MULTIPLE_CHOICE);

        demoOption01 = new Option("Blue", true);
        Option demoOption02 = new Option("Red", false);
        Option demoOption03 = new Option("Yellow", false);
        Option demoOption04 = new Option("Grey", true);

        List<Option> demoQuestion01Options = List.of(demoOption01, demoOption02, demoOption03, demoOption04);
        demoQuestion01.setListOfOptions(demoQuestion01Options);

        List<Question> listOfDemoQuestions = List.of(demoQuestion01);
        demoQuiz.setListOfQuestions(listOfDemoQuestions);

        List<Quiz> demoListOfQuizzes = List.of(demoQuiz);
        demoUser.setListOfQuizzes(demoListOfQuizzes);
        User savedUser = userRepository.save(demoUser);
    }

    @After
    public void cleanUp() {
        System.out.println("After every test");
        userRepository.delete(demoUser);
    }

    @Test
    public void testSubmitParticipantResponseSuccessful() {

        PostOptionDTO postOptionDTO = new PostOptionDTO();
        postOptionDTO.setOptionId(demoOption01.getId());
        List<PostOptionDTO> listOfPostOptionDTOS = new ArrayList<>();

        AnswerDTO answerDTO = new AnswerDTO();
        answerDTO.setQuestionId(demoQuestion01.getId());
        answerDTO.setListOfPostOptionDTOs(listOfPostOptionDTOS);
        List<AnswerDTO> listOfAnswerDTOS = new ArrayList<>();

        PostParticipantResponseDTO pPRDTO = new PostParticipantResponseDTO();
        pPRDTO.setQuizId(demoQuiz.getId());
        pPRDTO.setParticipantEmail("partemail");
        pPRDTO.setListOfAnswerDTOs(listOfAnswerDTOS);

        ParticipantResponse participantResponse = participantResponseServiceImpl.submitParticipantResponse(pPRDTO);
        Assert.assertNotNull("partemail", participantResponse.getParticipantEmail());

        participantResponseRepository.delete(participantResponse);
        // need to delete participant response otherwise when user is deleted in cleanUp, this also deletes foreign keys saved in participant response causing an error
    }




}
