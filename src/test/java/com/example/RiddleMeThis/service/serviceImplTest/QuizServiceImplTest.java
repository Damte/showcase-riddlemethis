package com.example.RiddleMeThis.service.serviceImplTest;

import com.example.RiddleMeThis.dto.QuizDTO;
import com.example.RiddleMeThis.dto.ResponseQuizDTO;
import com.example.RiddleMeThis.entities.*;
import com.example.RiddleMeThis.repository.UserRepository;
import com.example.RiddleMeThis.service.QuizService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class QuizServiceImplTest {

    @Autowired
    private QuizService quizService;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void before() {
        User demoUser = new User(new Date(), new Date(), "Poncho", "darknight@gotham.com", "batcave");
        Quiz demoQuiz = new Quiz(new Date(), new Date(), "demo quiz 2", demoUser);
        demoQuiz.setUuid();

        Question demoQuestion01 = new Question(demoQuiz, "What is the colour of the sky", QuestionType.MULTIPLE_CHOICE);

        Option demoOption01 = new Option("Blue", true);
        Option demoOption02 = new Option("Red", false);
        Option demoOption03 = new Option("Yellow", false);
        Option demoOption04 = new Option("Grey", true);

        List<Option> demoQuestion01Options = List.of(demoOption01, demoOption02, demoOption03, demoOption04);
        demoQuestion01.setListOfOptions(demoQuestion01Options);

        Question demoQuestion02 = new Question(demoQuiz, "Is the sun hot?", QuestionType.BINARY);
        Option demoOption05 = new Option("Yes", true);
        Option demoOption06 = new Option("No", false);

        List<Option> demoQuestion02Options = List.of(demoOption05, demoOption06);
        demoQuestion02.setListOfOptions(demoQuestion02Options);

        Question demoQuestion03 = new Question(demoQuiz, "Is Tallinn in Estonia?", QuestionType.BINARY);

        Option demoOption07 = new Option("Yes", true);
        Option demoOption08 = new Option("no", false);

        List<Option> demoQuestion03Options = List.of(demoOption07, demoOption08);
        demoQuestion03.setListOfOptions(demoQuestion03Options);

        List<Question> listOfDemoQuestions = List.of(demoQuestion01, demoQuestion02, demoQuestion03);
        demoQuiz.setListOfQuestions(listOfDemoQuestions);

        List<Quiz> demoListOfQuizzes = List.of(demoQuiz);
        demoUser.setListOfQuizzes(demoListOfQuizzes);
        userRepository.save(demoUser);

        System.out.println("Before every test");
    }

    @After
    public void cleanUp() {
        userRepository.delete(userRepository.findUserByName("Poncho"));
        System.out.println("After every test");

    }

    @Test
    public void testFindQuizzesByUserSuccess() {
        //given
        List<QuizDTO> dtoList = quizService.findQuizzesByUserId(userRepository.findUserByName("Poncho").getId());
        //then
        //when
        Assert.assertTrue(dtoList.size()>0);
    }

    @Test(expected = NullPointerException.class)
    public void testFindQuizzesByUserFails() {
        //given
        List<QuizDTO> dtoList = quizService.findQuizzesByUserId(0L);

        //then
        //when
        Assert.assertTrue(dtoList.size()>0);

    }

    @Test
    public void testFindAllUsersSuccess() {
        List<QuizDTO> quizDTOList = quizService.findAll();
        Assert.assertTrue(quizDTOList.size()>1);
//        Assert.assertFalse(quizDTOList.isEmpty());
    }
    @Test
    public void testFindQuizByUUIDSuccess() {
        //given
        //when
        ResponseQuizDTO responseQuizDTO = quizService.findQuizByUuid("123");
        //then
        Assert.assertNotNull(responseQuizDTO);
        Assert.assertNotNull(responseQuizDTO.getName());
    }
    @Test(expected = NullPointerException.class)
    public void testFindQuizByUUIDFail() {
        //given
        //when
        ResponseQuizDTO responseQuizDTO = quizService.findQuizByUuid("randomUuid");
        //then
    }
    @Test
    public void testCheckIfQuizAlreadyExists() {
        //given
        //when
        Boolean bool = quizService.checkIfQuizAlreadyExists(1L);
        Boolean bool2 = quizService.checkIfQuizAlreadyExists(100L);
        //then
        Assert.assertTrue(bool);
        Assert.assertFalse(bool2);
    }


}


