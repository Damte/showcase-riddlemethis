package com.example.RiddleMeThis.service.serviceImplTest;


import com.example.RiddleMeThis.converter.QuizConverter;
import com.example.RiddleMeThis.converter.UserConverter;
import com.example.RiddleMeThis.dto.UserDTO;
import com.example.RiddleMeThis.entities.User;
import com.example.RiddleMeThis.repository.UserRepository;
import com.example.RiddleMeThis.service.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter userConverter;

    private UserDTO user;
    private UserDTO savedUser;
    private List <UserDTO> dtoList;

    @Before
    public void before() {
        user = new UserDTO();
        user.setName("stan");
        user.setPassword("123");
        user.setEmail("dev.gmail.com");
        userConverter.convertUserDTOIntoUserModel(user);
        savedUser = userService.saveUser(user);
    }

    @After
    public void cleanUp() {
        userRepository.delete(userRepository.findUserByName("stan"));
    }

    @Test
    public void testSaveUserSuccess() {
        //given

        //then
        assertNotNull(savedUser.getId());
        assertEquals("dev.gmail.com", savedUser.getEmail());
        //when
    }

    @Test
    public void testFindAllUsersSuccess() {

        dtoList = userService.findAllUsers();
        Assert.assertTrue(dtoList.size()>1);
        Assert.assertFalse(dtoList.isEmpty());
    }

}
