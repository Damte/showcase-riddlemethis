package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.dto.ResponseQuizDTO;
import com.example.RiddleMeThis.entities.*;
import com.example.RiddleMeThis.repository.QuizRepository;
import com.example.RiddleMeThis.repository.UserRepository;
import com.example.RiddleMeThis.service.serviceImpl.QuizServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TestQuizService {

    @Autowired
    private QuizServiceImpl quizService;
    @Autowired
    private QuizRepository quizRepository;
    @Autowired
    private UserRepository userRepository;

    String generatedUUID;

    @Before
    public void setUp(){
        User user = new User();
        user.setName("User Name");
        user.setEmail("email");
        user.setPassword("pass");
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        userRepository.save(user);

        Quiz quiz = new Quiz();

        Question question = new Question();
        Option option = new Option();
        List<Question> questionList = new ArrayList<>();
        List<Option> optionList = new ArrayList<>();
        optionList.add(option);
        question.setListOfOptions(optionList);
        question.setQuiz(quiz);
        questionList.add(question);

        quiz.setName("quizTest");
        quiz.setUuid();
        generatedUUID = quiz.getQuizUUID();
        quiz.setActive(true);
        quiz.setCreatedAt(new Date());
        quiz.setUpdatedAt(new Date());
        quiz.setUser(userRepository.findUserById(1L));
        quiz.setListOfQuestions(questionList);

        List<Quiz> quizzes = new ArrayList<>();
        quizzes.add(quizRepository.save(quiz));
        user.setListOfQuizzes(quizzes);
        userRepository.save(user);

    }

    @Test
    public void testFindByUuidResultSuccess(){
        ResponseQuizDTO responseQuizDTO = quizService.findQuizByUuid(generatedUUID);
        assertNotNull(responseQuizDTO.getName());
    }
}
