package com.example.RiddleMeThis.controller;

import com.example.RiddleMeThis.dto.UserDTO;
import com.example.RiddleMeThis.entities.JwtRequest;
import com.example.RiddleMeThis.entities.JwtResponse;
import com.example.RiddleMeThis.exception.EmailValidationException;
import com.example.RiddleMeThis.service.JwtUserDetailsService;
import com.example.RiddleMeThis.service.UserService;
import com.example.RiddleMeThis.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserProfileController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService userDetailsService;
    private final UserService userService;

    @Autowired
    public UserProfileController(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, JwtUserDetailsService userDetailsService, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
    }

    @PostMapping(value = "/user")
    public ResponseEntity<UserDTO> createUserProfile(@RequestBody UserDTO user) {

        try {
            if (userService.validateEmail(user)) {
                throw new EmailValidationException("This email: " + user.getEmail() + " is invalid");
            } else {
                UserDTO saveUser = userService.saveUser(user);
                return new ResponseEntity<>(saveUser, HttpStatus.ACCEPTED);
            }
        } catch (EmailValidationException emailValidationException) {
            return new ResponseEntity(emailValidationException, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/users")
    public ResponseEntity<UserDTO> getUserList() {
        Iterable<UserDTO> userList = userService.findAllUsers();
        return new ResponseEntity(userList, HttpStatus.OK);
    }


    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getName(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getName());

        final String token = jwtTokenUtil.generateToken(userDetails);

        final Long userId = userService.findUserByUsername(authenticationRequest.getName()).getId();
        JwtResponse jwtResponse = new JwtResponse(token, userId);
        return ResponseEntity.ok(jwtResponse);
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}