package com.example.RiddleMeThis.controller;

import com.example.RiddleMeThis.dto.*;
import com.example.RiddleMeThis.entities.ParticipantResponse;
import com.example.RiddleMeThis.entities.Quiz;
import com.example.RiddleMeThis.exception.*;
import com.example.RiddleMeThis.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class QuizController {

    private QuizService quizService;


    @Autowired
    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @PostMapping(value = "/new-quiz") // It's not necessary to collect the UserID from the path variable, but should this be included?
    public ResponseEntity<QuizCreationResponseDTO> createNewQuiz(@RequestBody QuizCreationRequestDTO newQuiz) {
        try {
            if (!quizService.checkUserExists(newQuiz.getUserId())) {
                throw new UserDoesntExist("There is no user with Id: " + newQuiz.getUserId());
            } else {
                QuizCreationResponseDTO savedQuiz = quizService.saveQuiz(newQuiz);
                return new ResponseEntity<>(savedQuiz, HttpStatus.CREATED);
            }
        } catch (UserDoesntExist userDoesntExist) {
            return new ResponseEntity(userDoesntExist, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/user/{id}/quizzes/responses")
    public ResponseEntity<List<QuizStatisticsRequestDTO>> findNumberOfParticipantsByQuiz(@PathVariable("id") Long userId) {
        try {
            if (!quizService.checkUserExists(userId)) {
                throw new UserDoesntExist("There is no user with Id: " + userId);
            } else {
                List<QuizStatisticsRequestDTO> statistics = quizService.findQuizStatisticsByUserId(userId);
                return new ResponseEntity<>(statistics, HttpStatus.OK);
            }
        } catch (UserDoesntExist e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/quizzes")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<QuizDTO>> findAllQuizzes() {
        List<QuizDTO> quizDTOList = quizService.findAll();
        return new ResponseEntity<>(quizDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "quiz/{uuid}")
    public ResponseEntity getQuizByUUID(@PathVariable("uuid") String uuid) {
        try {
            ResponseQuizDTO responseQuizDTO = quizService.findQuizByUuid(uuid.toLowerCase());
            return new ResponseEntity(responseQuizDTO, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity(new QuizNotFoundException("Cannot find quiz with UUID: " + uuid), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "user/{userId}/quizzes")
    public ResponseEntity<List<QuizDTO>> getQuizBySingleUser(@PathVariable("userId") Long userId) {
        try {
            if (quizService.checkUserExists(userId)) {
                List<QuizDTO> quizDTOS = quizService.findQuizzesByUserId(userId);
                return new ResponseEntity(quizDTOS, HttpStatus.FOUND);
            } else {
                    throw new UserDoesntExist("There is no user with Id: " + userId);
            }
        } catch (UserDoesntExist e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }

}
