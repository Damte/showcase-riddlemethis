package com.example.RiddleMeThis.controller;

import com.example.RiddleMeThis.dto.PostParticipantResponseDTO;
import com.example.RiddleMeThis.entities.ParticipantResponse;
import com.example.RiddleMeThis.exception.QuizNotFoundException;
import com.example.RiddleMeThis.service.ParticipantResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class ParticipantResponseController {

    private final ParticipantResponseService participantResponseService;

    @Autowired
    public ParticipantResponseController(ParticipantResponseService participantResponseService) {
        this.participantResponseService = participantResponseService;
    }

    @PostMapping(value = "/quiz/response")
    public ResponseEntity<ParticipantResponse> submitResponseToQuiz(@RequestBody PostParticipantResponseDTO dto) {
        try {
            if (!participantResponseService.checkIfQuizExists(dto.getQuizId())) {
                throw new QuizNotFoundException("There is no quiz with ID " + dto.getQuizId());
            } else {
                ParticipantResponse participantResponse = participantResponseService.submitParticipantResponse(dto);
                return new ResponseEntity<>(participantResponse, HttpStatus.CREATED);
            }
        } catch (QuizNotFoundException quizNotFoundException) {
            return new ResponseEntity(quizNotFoundException, HttpStatus.NOT_FOUND);
        }
    }

    // TODO:  Implement the DTO object (instead of the current model Object)
    // TODO: Check if id exists, and if it belongs to the quiz and so on.
    // TODO: Solve this using a custom query on the repository. Refactor logic, so controller, service and repository work as they are intended.
 /*   @GetMapping(value = "/response/{id}/score")
    public ResponseEntity<Double> findScoreBySubmittedParticipantResponse(@PathVariable("id") Long participantResponseId) {
        ParticipantResponse participantResponse = participantResponseService.findParticipantResponseById(participantResponseId);
        List<Question> listOfQuestions = participantResponse.getQuiz().getListOfQuestions();
        Integer numberOfQuestions = listOfQuestions.size();
        List<Option> correctOptions = new ArrayList<>();
        listOfQuestions.stream().forEach(question -> question.getListOfOptions().stream().filter(option -> option.getCorrect().equals(true)).forEach(option -> correctOptions.add(option)));
        List<Answer> listOfCorrectAnswers = participantResponse.getListOfAnswers()
                .stream().filter(s -> s.getListOfOptions()
                        .stream().allMatch(option -> option.getCorrect())).collect(Collectors.toList());
        Integer questionsAnsweredCorrectly = 0;
        for (Question q : listOfQuestions) {
            List<Option> correctOptionsByQuestion = q.getListOfOptions().stream().filter(Option::getCorrect).collect(Collectors.toList());
            List<Option> optionsByQuestionByParticipantResponse = new ArrayList<>();
            listOfCorrectAnswers
                    .stream().filter(answer -> answer.getQuestionId().equals(q.getId())).forEach(answer -> answer.getListOfOptions()
                    .stream().forEach(option -> optionsByQuestionByParticipantResponse.add(option)));
            if (correctOptionsByQuestion.containsAll(optionsByQuestionByParticipantResponse) && correctOptionsByQuestion.size() == optionsByQuestionByParticipantResponse.size()) {
                questionsAnsweredCorrectly++;
            }
        }
        return new ResponseEntity(questionsAnsweredCorrectly + " of " + numberOfQuestions, HttpStatus.OK);
    }*/
}
