package com.example.RiddleMeThis.exception;

public class QuizNotFoundException extends Throwable {
    public QuizNotFoundException(String s) {
        super(s);
    }
}
