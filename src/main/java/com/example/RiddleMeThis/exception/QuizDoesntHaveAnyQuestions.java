package com.example.RiddleMeThis.exception;

public class QuizDoesntHaveAnyQuestions extends Throwable {
    public QuizDoesntHaveAnyQuestions(String s) {
        super(s);
    }

}
