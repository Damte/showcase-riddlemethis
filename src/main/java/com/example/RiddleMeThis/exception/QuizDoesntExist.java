package com.example.RiddleMeThis.exception;

public class QuizDoesntExist extends Throwable {
    public QuizDoesntExist(String s) {
        super(s);
    }
}
