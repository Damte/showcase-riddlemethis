package com.example.RiddleMeThis.exception;

public class QuizHasNotBeenAnsweredYet extends Throwable {
    public QuizHasNotBeenAnsweredYet(String s) {
        super(s);
    }
}
