package com.example.RiddleMeThis.exception;

public class EmailValidationException extends Throwable{
    public EmailValidationException(String s) {
        super(s);
    }
}
