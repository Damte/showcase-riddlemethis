package com.example.RiddleMeThis.exception;

public class QuizAlreadyExists extends Throwable {
    public QuizAlreadyExists(String s) {
        super(s);
    }

}
