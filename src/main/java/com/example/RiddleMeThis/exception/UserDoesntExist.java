package com.example.RiddleMeThis.exception;

public class UserDoesntExist extends Throwable {
    public UserDoesntExist(String s) {
        super(s);
    }

}
