package com.example.RiddleMeThis.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Quiz extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "quiz")
    private List<ParticipantResponse> listOfParticipantResponse;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "quiz")
    private List<Question> listOfQuestions;

    @Column(nullable = false)
    private String quizUUID;

    public Quiz(User user, String name) {
        this.user = user;
        this.name = name;
    }

    public Quiz(Date createdAt, Date updatedAt, String name, User user) {
        super(createdAt, updatedAt);
        this.name = name;
        this.user = user;
    }

    public Quiz() {
    }

    public String getQuizUUID() {
        return quizUUID;
    }

    public void setUuid() {
        this.quizUUID = UUID.randomUUID().toString();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ParticipantResponse> getListOfParticipantResponse() {
        return listOfParticipantResponse;
    }

    public void setListOfParticipantResponse(List<ParticipantResponse> listOfParticipantResponse) {
        this.listOfParticipantResponse = listOfParticipantResponse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getListOfQuestions() {
        return listOfQuestions;
    }

    public void setListOfQuestions(List<Question> listOfQuestions) {
        this.listOfQuestions = listOfQuestions;
    }
}
