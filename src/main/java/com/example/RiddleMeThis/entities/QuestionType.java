package com.example.RiddleMeThis.entities;

public enum QuestionType {
    BINARY,
    MULTIPLE_CHOICE;

}
