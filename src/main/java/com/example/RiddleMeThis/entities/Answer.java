package com.example.RiddleMeThis.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long questionId;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    private List<Option> listOfOptions;

    public Answer(Long questionId, List<Option> listOfOptions) {
        this.questionId = questionId;
        this.listOfOptions = listOfOptions;
    }

    public Answer(Long questionId) {
        this.questionId = questionId;
    }

    public Answer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public List<Option> getListOfOptions() {
        return listOfOptions;
    }

    public void setListOfOptions(List<Option> listOfOptions) {
        this.listOfOptions = listOfOptions;
    }
}
