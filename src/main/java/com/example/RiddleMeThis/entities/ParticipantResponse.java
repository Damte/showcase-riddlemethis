package com.example.RiddleMeThis.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class ParticipantResponse extends AuditModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String participantEmail;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "participantResponse_id")
    private List<Answer> listOfAnswers;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "quiz_id",nullable = false)
    private Quiz quiz;

    public ParticipantResponse(String participantEmail, Quiz quiz) {
        this.participantEmail = participantEmail;
        this.quiz = quiz;
    }

    public ParticipantResponse(String participantEmail, List<Answer> listOfAnswers) {
        this.participantEmail = participantEmail;
        this.listOfAnswers = listOfAnswers;
    }

    public ParticipantResponse() {
    }

    public ParticipantResponse(Date createdAt, Date updatedAt, String participantEmail, Quiz quiz) {
        super(createdAt, updatedAt);
        this.participantEmail = participantEmail;
        this.quiz = quiz;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantEmail() {
        return participantEmail;
    }

    public void setParticipantEmail(String participantEmail) {
        this.participantEmail = participantEmail;
    }

    public List<Answer> getListOfAnswers() {
        return listOfAnswers;
    }

    public void setListOfAnswers(List<Answer> listOfAnswers) {
        this.listOfAnswers = listOfAnswers;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

}
