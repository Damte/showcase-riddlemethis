package com.example.RiddleMeThis.repository;

import com.example.RiddleMeThis.entities.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {
    List<Question> findQuestionsByQuizId(Long QuizId);
}
