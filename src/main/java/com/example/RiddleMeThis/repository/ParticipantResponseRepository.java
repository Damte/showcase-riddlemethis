package com.example.RiddleMeThis.repository;

import com.example.RiddleMeThis.entities.ParticipantResponse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantResponseRepository extends CrudRepository<ParticipantResponse, Long> {
}
