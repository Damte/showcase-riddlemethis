package com.example.RiddleMeThis.repository;

import com.example.RiddleMeThis.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findUserById(Long id);
    User findUserByName(String name);
    User findByName(String name);
    boolean existsById(Long id);
    boolean existsByEmail(String email);
}
