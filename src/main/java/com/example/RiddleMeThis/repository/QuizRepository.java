package com.example.RiddleMeThis.repository;

import com.example.RiddleMeThis.entities.Quiz;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuizRepository extends CrudRepository<Quiz, Long> {
    Quiz findByQuizUUID(String quizUUID);
    List<Quiz> findAllByUserId(Long userId);
}
