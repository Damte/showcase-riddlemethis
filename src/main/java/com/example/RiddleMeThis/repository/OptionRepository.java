package com.example.RiddleMeThis.repository;

import com.example.RiddleMeThis.entities.Option;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionRepository extends CrudRepository<Option, Long> {
}
