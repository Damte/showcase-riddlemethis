package com.example.RiddleMeThis.service.serviceImpl;

import com.example.RiddleMeThis.converter.QuizConverter;
import com.example.RiddleMeThis.dto.*;
import com.example.RiddleMeThis.entities.Quiz;
import com.example.RiddleMeThis.repository.QuizRepository;
import com.example.RiddleMeThis.service.QuizService;
import com.example.RiddleMeThis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {

    private final QuizRepository quizRepository;
    private final QuizConverter quizConverter;
    private final UserService userService;


    @Autowired
    public QuizServiceImpl(QuizRepository quizRepository, QuizConverter quizConverter, UserService userService) {
        this.quizRepository = quizRepository;
        this.quizConverter = quizConverter;
        this.userService = userService;
    }

    @Override
    public QuizCreationResponseDTO saveQuiz(QuizCreationRequestDTO dto) {
        Quiz quizToBeSaved = quizConverter.toQuizOnCreation(dto);
        quizToBeSaved.setUser(userService.findUserModelById(dto.getUserId()));
        quizToBeSaved.setCreatedAt(new Date());
        quizToBeSaved.setUpdatedAt(new Date());
        quizToBeSaved.setUuid();
        quizRepository.save(quizToBeSaved);
        return quizConverter.toQuizResponseDTOOnCreation(quizToBeSaved);
    }

    @Transactional
    @Override
    public List<QuizDTO> findAll() {
        List<QuizDTO> savedQuizzes = new ArrayList<>();
        quizRepository.findAll().forEach(quiz -> savedQuizzes.add(quizConverter.convertQuizModelIntoQuizDTO(quiz)));
        return savedQuizzes;
    }

    @Override
    public Quiz findQuizById(Long quizId) {
        return quizRepository.findById(quizId).get();
    }

    @Override
    public Boolean checkIfQuizAlreadyExists(Long quizId) {
        return quizRepository.existsById(quizId);
    }

    @Transactional
    @Override
    public ResponseQuizDTO findQuizByUuid(String quizUUID) {
        Quiz quiz = quizRepository.findByQuizUUID(quizUUID);
        return quizConverter.convertQuizToQuizDTOForResponse(quiz);
    }

    @Transactional
    @Override
    public List<QuizDTO> findQuizzesByUserId(Long userId) {
        List<Quiz> listOfQuizzesByUser = quizRepository.findAllByUserId(userId);
        List<QuizDTO> listOfQuizDTOsByUser = new ArrayList<>();

        for (Quiz quiz : listOfQuizzesByUser) {
            listOfQuizDTOsByUser.add(quizConverter.convertQuizModelIntoQuizDTO(quiz));
        }
        return listOfQuizDTOsByUser;
    }

    @Override
    public Boolean checkUserExists(Long userId) {
        return userService.checkUserExists(userId);
    }

    @Override
    public List<QuizStatisticsRequestDTO> findQuizStatisticsByUserId(Long userId) {
        List<QuizStatisticsRequestDTO> listOfDTOs = new ArrayList<>();
        List<Quiz> listOfQuizzesByUser = quizRepository.findAllByUserId(userId);

        for (Quiz quiz : listOfQuizzesByUser) {
            listOfDTOs.add(quizConverter.toQuizStatisticsRequestDTO(quiz));
        }
        return listOfDTOs;
    }

}
