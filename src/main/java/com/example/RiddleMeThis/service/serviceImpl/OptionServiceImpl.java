package com.example.RiddleMeThis.service.serviceImpl;

import com.example.RiddleMeThis.entities.Option;
import com.example.RiddleMeThis.repository.OptionRepository;
import com.example.RiddleMeThis.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OptionServiceImpl implements OptionService {

    private final OptionRepository optionRepository;

    @Autowired
    public OptionServiceImpl(OptionRepository optionRepository) {
        this.optionRepository = optionRepository;
    }

    @Override
    public Option findById(Long optionId) {
        return optionRepository.findById(optionId).get();
    }
}
