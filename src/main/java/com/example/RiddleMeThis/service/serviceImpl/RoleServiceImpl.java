package com.example.RiddleMeThis.service.serviceImpl;

import com.example.RiddleMeThis.entities.Role;
import com.example.RiddleMeThis.repository.RoleRepository;
import com.example.RiddleMeThis.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    final RoleRepository roleRepository;

    @Autowired
    private RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role add(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role getByName(String name) {
        return roleRepository.findByUsername(name);
    }

    @Override
    public Role getRole(Long id) {
        return roleRepository.findById(id).filter(u -> u.getId().equals(id)).orElse(null);
    }

}