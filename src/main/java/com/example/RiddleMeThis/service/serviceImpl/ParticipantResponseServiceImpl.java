package com.example.RiddleMeThis.service.serviceImpl;

import com.example.RiddleMeThis.converter.ParticipantResponseConverter;
import com.example.RiddleMeThis.dto.PostParticipantResponseDTO;
import com.example.RiddleMeThis.entities.ParticipantResponse;
import com.example.RiddleMeThis.entities.Quiz;
import com.example.RiddleMeThis.repository.ParticipantResponseRepository;
import com.example.RiddleMeThis.service.ParticipantResponseService;
import com.example.RiddleMeThis.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class ParticipantResponseServiceImpl implements ParticipantResponseService {

    private final ParticipantResponseConverter participantResponseConverter;
    private final ParticipantResponseRepository participantResponseRepository;
    private final QuizService quizService;

    @Autowired
    public ParticipantResponseServiceImpl(ParticipantResponseConverter participantResponseConverter,
                                          ParticipantResponseRepository participantResponseRepository, QuizService quizService) {
        this.participantResponseConverter = participantResponseConverter;
        this.participantResponseRepository = participantResponseRepository;
        this.quizService = quizService;
    }

    @Transactional
    @Override
    public ParticipantResponse submitParticipantResponse(PostParticipantResponseDTO dto) {
        ParticipantResponse participantResponse = participantResponseConverter.toParticipantResponse(dto);
        participantResponse.setQuiz(quizService.findQuizById(dto.getQuizId()));
        participantResponse.setCreatedAt(new Date());
        participantResponse.setUpdatedAt(new Date());
        return participantResponseRepository.save(participantResponse);
    }

    @Override
    public boolean checkIfQuizExists(Long quizId) {
        return quizService.checkIfQuizAlreadyExists(quizId);
    }

    @Override
    public ParticipantResponse findParticipantResponseById(Long id) {
        return participantResponseRepository.findById(id).get();
    }

}
