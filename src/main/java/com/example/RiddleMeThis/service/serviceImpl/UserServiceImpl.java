package com.example.RiddleMeThis.service.serviceImpl;

import com.example.RiddleMeThis.converter.UserConverter;
import com.example.RiddleMeThis.dto.ResponseLoggedInUserDTO;
import com.example.RiddleMeThis.dto.UserDTO;
import com.example.RiddleMeThis.entities.User;
import com.example.RiddleMeThis.repository.UserRepository;
import com.example.RiddleMeThis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }


    @Override
    public UserDTO saveUser(UserDTO user) {
        User userToBeSaved = userConverter.convertUserDTOIntoUserModel(user);
        userToBeSaved.setCreatedAt(new Date());
        userToBeSaved.setUpdatedAt(new Date());
        userToBeSaved.setActive(true);
        userRepository.save(userToBeSaved);
        return userConverter.convertUserModelIntoUserDTO(userToBeSaved);
    }

    @Override
    public Boolean validateEmail(UserDTO userDTO) {
        return userRepository.existsByEmail(userDTO.getEmail());
    }

    @Override
    public List<UserDTO> findAllUsers() {
        List<UserDTO> savedUserDTOs = new ArrayList<>();
        userRepository.findAll().forEach(user -> savedUserDTOs.add(userConverter.convertUserModelIntoUserDTO(user)));
        return savedUserDTOs;
    }

    @Override
    public Boolean checkUserExists(Long userId) {
        return userRepository.existsById(userId);
    }

    @Override
    public User findUserModelById(Long userId) {
        return userRepository.findById(userId).get();
    }

    @Override
    public User findUserByUsername(String name) {
        return userRepository.findByName(name);
    }


//    @Override
//    public ResponseLoggedInUserDTO loadFakeUserLoginDetails(Long userId) {
//        return userConverter.toResponseLoggedInUserDTO(userRepository.findUserById(userId));
//    }

}
