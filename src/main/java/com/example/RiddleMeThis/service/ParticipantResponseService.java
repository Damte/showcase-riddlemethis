package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.dto.PostParticipantResponseDTO;
import com.example.RiddleMeThis.entities.ParticipantResponse;

public interface ParticipantResponseService {
    ParticipantResponse submitParticipantResponse(PostParticipantResponseDTO dto);
    boolean checkIfQuizExists(Long quizId);
    ParticipantResponse findParticipantResponseById(Long id);
}
