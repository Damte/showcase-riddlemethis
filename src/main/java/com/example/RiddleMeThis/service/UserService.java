package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.dto.UserDTO;
import com.example.RiddleMeThis.entities.User;

import java.util.List;

public interface UserService {
    UserDTO saveUser(UserDTO user);

    List<UserDTO> findAllUsers();

    Boolean validateEmail(UserDTO email);

    User findUserModelById(Long userId);

    User findUserByUsername(String name);

    //    ResponseLoggedInUserDTO loadFakeUserLoginDetails(Long userId);
    Boolean checkUserExists(Long userId);
}
