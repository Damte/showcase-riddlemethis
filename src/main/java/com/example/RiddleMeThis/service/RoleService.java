package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.entities.Role;

public interface RoleService {
    Role getRole(Long id);
    Role add(Role role);
    Role getByName(String name );
}