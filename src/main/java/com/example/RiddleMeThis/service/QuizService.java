package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.dto.*;
import com.example.RiddleMeThis.entities.Quiz;

import java.util.List;

public interface QuizService {
    QuizCreationResponseDTO saveQuiz(QuizCreationRequestDTO dto);
    List<QuizDTO> findAll();
    Quiz findQuizById(Long quizId);
    Boolean checkIfQuizAlreadyExists(Long quizId);
    ResponseQuizDTO findQuizByUuid(String uuid);
    List<QuizDTO> findQuizzesByUserId(Long id);
    Boolean checkUserExists(Long userId);
    List<QuizStatisticsRequestDTO> findQuizStatisticsByUserId(Long userId);
}
