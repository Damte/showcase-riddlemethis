package com.example.RiddleMeThis.service;

import com.example.RiddleMeThis.entities.Option;

public interface OptionService {
    Option findById(Long optionId);
}

