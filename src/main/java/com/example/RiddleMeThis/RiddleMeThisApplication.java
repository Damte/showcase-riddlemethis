package com.example.RiddleMeThis;

import com.example.RiddleMeThis.entities.*;
import com.example.RiddleMeThis.repository.ParticipantResponseRepository;
import com.example.RiddleMeThis.repository.RoleRepository;
import com.example.RiddleMeThis.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import com.example.RiddleMeThis.repository.OptionRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;

@SpringBootApplication
@EnableSwagger2
public class RiddleMeThisApplication implements CommandLineRunner {

	private ParticipantResponseRepository participantResponseRepository;
	private UserRepository userRepository;
	private OptionRepository optionRepository;
	private RoleRepository roleRepository;
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	public RiddleMeThisApplication(UserRepository userRepository, ParticipantResponseRepository participantResponseRepository, OptionRepository optionRepository, RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.participantResponseRepository = participantResponseRepository;
		this.optionRepository = optionRepository;
		this.roleRepository = roleRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(RiddleMeThisApplication.class, args);
	}

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.example.RiddleMeThis")).build();
    }

    @Override
    public void run(String... args) throws Exception {
		// Creating roles
		Role role = new Role();
		role.setName("ADMIN");
		roleRepository.save(role);
		Role role2 = new Role();
		role2.setName("USER");
		roleRepository.save(role2);

		// Stanly's command line runner code
        User user = new User(new Date(), new Date(), "stanley", "stanley.ezeoke", "$2a$10$KEn4FtIgaeAIKI99ciQR7OpHEGPaHiYkjXgEl3WCasfdaA3s1pLEy");
        User user2 = new User(new Date(), new Date(), "king", "ify@gmail", "$2a$10$KEn4FtIgaeAIKI99ciQR7OpHEGPaHiYkjXgEl3WCasfdaA3s1pLEy");
        // actual password: 456

        user.setRole(role);
        user2.setRole(role2);

        Quiz quiz = new Quiz(new Date(), new Date(), "java", user);
        Quiz quiz2 = new Quiz(new Date(), new Date(), "facebook", user2);
        quiz.setUuid();
        quiz2.setUuid();
        Quiz quiz3 = new Quiz(new Date(), new Date(), "sda",user2);
        quiz3.setUuid();

		Question demoQuestionA = new Question(quiz, "What is the colour of the skys", QuestionType.MULTIPLE_CHOICE);

		Option demoOptionA = new Option("Blue", true);
		Option demoOptionB = new Option("Red", false);
		Option demoOptionC = new Option("Yellow", false);
		Option demoOptionD = new Option("Grey", true);

		List<Option> demoQuestionAOptions = List.of(demoOptionA, demoOptionB, demoOptionC, demoOptionD);
		demoQuestionA.setListOfOptions(demoQuestionAOptions);

        List<Quiz> quizList = List.of(quiz);
        user.setListOfQuizzes(quizList);

        List<Quiz> quizList2 = List.of(quiz2, quiz3);
        user2.setListOfQuizzes(quizList2);

        userRepository.save(user);
        userRepository.save(user2);

		// Brendan's command line runner code

		// --------------------- Creating quiz 1 ---------------------
		User demoUser = new User(new Date(), new Date(), "Batman", "darknight@gotham.com", "$2a$10$KEn4FtIgaeAIKI99ciQR7OpHEGPaHiYkjXgEl3WCasfdaA3s1pLEy");
		demoUser.setRole(role);

		Quiz demoQuiz = new Quiz(new Date(), new Date(), "demo quiz", demoUser);
		demoQuiz.setUuid();

		Question demoQuestion01 = new Question(demoQuiz, "What is the colour of the sky", QuestionType.MULTIPLE_CHOICE);

		Option demoOption01 = new Option("Blue", true);
		Option demoOption02 = new Option("Red", false);
		Option demoOption03 = new Option("Yellow", false);
		Option demoOption04 = new Option("Grey", true);

		List<Option> demoQuestion01Options = List.of(demoOption01, demoOption02, demoOption03, demoOption04);
		demoQuestion01.setListOfOptions(demoQuestion01Options);

		Question demoQuestion02 = new Question(demoQuiz, "Is the sun hot?", QuestionType.BINARY);
		Option demoOption05 = new Option("Yes", true);
		Option demoOption06 = new Option("No", false);

		List<Option> demoQuestion02Options = List.of(demoOption05, demoOption06);
		demoQuestion02.setListOfOptions(demoQuestion02Options);

		Question demoQuestion03 = new Question(demoQuiz, "Is Tallinn in Estonia?", QuestionType.BINARY);

		Option demoOption07 = new Option("Yes", true);
		Option demoOption08 = new Option("no", false);

		List<Option> demoQuestion03Options = List.of(demoOption07, demoOption08);
		demoQuestion03.setListOfOptions(demoQuestion03Options);

		List<Question> listOfDemoQuestions = List.of(demoQuestion01, demoQuestion02, demoQuestion03);
		demoQuiz.setListOfQuestions(listOfDemoQuestions);

		// --------------------- Creating quiz 2 ---------------------

		Quiz demoQuiz2 = new Quiz(new Date(), new Date(), "demo quiz2", demoUser);
		demoQuiz2.setUuid();

		Question demoQuestion04 = new Question(demoQuiz2, "Which is the God of war?", QuestionType.MULTIPLE_CHOICE);

		Option demoOption10 = new Option("Tyr", true);
		Option demoOption20 = new Option("Ares", true);
		Option demoOption25 = new Option("Poseidon", false);

		List<Option> demoQuestion04Options = List.of(demoOption10, demoOption20, demoOption25);
		demoQuestion04.setListOfOptions(demoQuestion04Options);

		Question demoQuestion05 = new Question(demoQuiz2, "Is the sky the limit?", QuestionType.BINARY);
		Option demoOption30 = new Option("Yes", true);
		Option demoOption40 = new Option("No", false);

		List<Option> demoQuestion05Options = List.of(demoOption30, demoOption40);
		demoQuestion05.setListOfOptions(demoQuestion05Options);

		List<Question> listOfDemoQuestions2 = List.of(demoQuestion04, demoQuestion05);
		demoQuiz2.setListOfQuestions(listOfDemoQuestions2);


//		List<Quiz> demoListOfQuizzes = List.of(demoQuiz2);
		List<Quiz> demoListOfQuizzes = List.of(demoQuiz, demoQuiz2);

		demoUser.setListOfQuizzes(demoListOfQuizzes);
		userRepository.save(demoUser);

////		 --------------------- participant response 1  ---------------------
//		ParticipantResponse participantResponse1 = new ParticipantResponse(new Date(), new Date(), "Liene@sda.com", demoQuiz);
//
//		Answer Answer1= new Answer(demoQuestion01.getId(), List.of(optionRepository.findById(demoOption04.getId()).get(), optionRepository.findById(demoOption01.getId()).get()));
//		Answer Answer2 = new Answer(demoQuestion02.getId(), List.of(optionRepository.findById(demoOption05.getId()).get()));
//		Answer Answer3 = new Answer(demoQuestion03.getId(), List.of(optionRepository.findById(demoOption07.getId()).get()));
//
//		List<Answer> answersOfParticipant10 = List.of(answer1, answer2, answer3);
//
//		participantResponseRepository.save(participantResponse1);
//
//		participantResponse1.setListOfAnswers(answersOfParticipant10);
//		participantResponseRepository.save(participantResponse1);
//
//		// --------------------- participant response 2  ---------------------
//		ParticipantResponse participantResponse2 = new ParticipantResponse(new Date(), new Date(), "Mahdad@sda.com", demoQuiz);
//
//		Answer answer4 = new Answer(demoQuestion01.getId(), List.of(optionRepository.findById(demoOption03.getId()).get()));
//		Answer answer5 = new Answer(demoQuestion02.getId(), List.of(optionRepository.findById(demoOption06.getId()).get()));
//		Answer answer6 = new Answer(demoQuestion03.getId(), List.of(optionRepository.findById(demoOption08.getId()).get()));
//
//		List<Answer> answersOfParticipant2 = List.of(answer4, answer5, answer6);
//		participantResponseRepository.save(participantResponse2);
//
//		participantResponse2.setListOfAnswers(answersOfParticipant2);
//		participantResponseRepository.save(participantResponse2);
	}
}
