package com.example.RiddleMeThis.converter;

import com.example.RiddleMeThis.dto.OptionCreationRequestDTO;
import com.example.RiddleMeThis.dto.OptionDTO;
import com.example.RiddleMeThis.dto.PostOptionDTO;
import com.example.RiddleMeThis.dto.ResponseOptionListDTO;
import com.example.RiddleMeThis.entities.Option;
import com.example.RiddleMeThis.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OptionConverter {

    private OptionService optionService;

    @Autowired
    public OptionConverter(OptionService optionService) {
        this.optionService = optionService;
    }

    public OptionConverter() {
    }

    public Option toOption(PostOptionDTO dto) {
        return optionService.findById(dto.getOptionId());
    }

    public PostOptionDTO toPostOptionDTO(Option option) {
        return new PostOptionDTO(option.getId());
    }

    public List<Option> toListOfOptions(List<PostOptionDTO> listOfOptionDTOs) {
        List<Option> listOfOptions = new ArrayList<>();
        for (PostOptionDTO dto : listOfOptionDTOs) {
            listOfOptions.add(toOption(dto));
        }
        return listOfOptions;
    }

    public List<PostOptionDTO> toListOfPostOptionDTOs(List<Option> listOfOptions) {
        List<PostOptionDTO> listOfPostOptionDTOS = new ArrayList<>();
        for (Option a : listOfOptions) {
            listOfPostOptionDTOS.add(toPostOptionDTO(a));
        }
        return listOfPostOptionDTOS;
    }

    public static OptionDTO convertOptionModelIntoOptionDTO(Option option) {
        OptionDTO optionDTO = new OptionDTO();
        optionDTO.setId(option.getId());
        optionDTO.setDescription(option.getDescription());
        optionDTO.setCorrect(option.getCorrect());
        return optionDTO;
    }

    public List<ResponseOptionListDTO> convertListOfQuestionsToDTOListForResponse(List<Option> optionList) {
        List<ResponseOptionListDTO> responseOptionDTOListListDTOList = new ArrayList<>();
        for (Option a : optionList) {
            ResponseOptionListDTO optDTO = convertOptionToOptionDTOForResponse(a);
            responseOptionDTOListListDTOList.add(optDTO);
        }
        return responseOptionDTOListListDTOList;
    }

    public ResponseOptionListDTO convertOptionToOptionDTOForResponse(Option option) {
        ResponseOptionListDTO responseOptionListDTO = new ResponseOptionListDTO();
        responseOptionListDTO.setDescription(option.getDescription());
        responseOptionListDTO.setCorrect(option.getCorrect());
        responseOptionListDTO.setId(option.getId());
        return responseOptionListDTO;
    }

    public Option toOptionOnCreation(OptionCreationRequestDTO optionDTO) {
        Option option = new Option();
        option.setDescription(optionDTO.getDescription());
        option.setCorrect(optionDTO.getCorrect());
        return option;
    }

    public List<Option> toListOfOptionsOnCreation(List<OptionCreationRequestDTO> listOfOptionDTOs) {
        List<Option> listOfOptions = new ArrayList<>();
        for (OptionCreationRequestDTO dto : listOfOptionDTOs) {
            listOfOptions.add(toOptionOnCreation(dto));
        }
        return listOfOptions;
    }
}
