package com.example.RiddleMeThis.converter;
import com.example.RiddleMeThis.dto.ResponseLoggedInUserDTO;
import com.example.RiddleMeThis.dto.UserDTO;
import com.example.RiddleMeThis.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {
private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserConverter(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User convertUserDTOIntoUserModel(UserDTO userDTO) { // TODO change the dto (front and back) not needed ID?
        User user = new User();
        user.setName(userDTO.getName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
//        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        return user;
    }

    public UserDTO convertUserModelIntoUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    public ResponseLoggedInUserDTO toResponseLoggedInUserDTO(User user) {
        return new ResponseLoggedInUserDTO(user.getId(), user.getName(), user.getEmail());
    }

}
