package com.example.RiddleMeThis.converter;

import com.example.RiddleMeThis.dto.PostParticipantResponseDTO;
import com.example.RiddleMeThis.entities.ParticipantResponse;
import com.example.RiddleMeThis.entities.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ParticipantResponseConverter {

    private final AnswerConverter answerConverter;

    @Autowired
    public ParticipantResponseConverter(AnswerConverter answerConverter) {
        this.answerConverter = answerConverter;
    }

    public ParticipantResponse toParticipantResponse(PostParticipantResponseDTO dto) {
        List<Answer> listOfAnswers = answerConverter.toListOfAnswers(dto.getListOfAnswerDTOs());
        return new ParticipantResponse(dto.getParticipantEmail(), listOfAnswers);
    }
}