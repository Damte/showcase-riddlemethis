package com.example.RiddleMeThis.converter;

import com.example.RiddleMeThis.dto.*;
import com.example.RiddleMeThis.entities.Option;
import com.example.RiddleMeThis.entities.Question;
import com.example.RiddleMeThis.entities.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.List;

@Component
public class QuestionConverter {

    private Quiz quiz;
    private final OptionConverter optionConverter;

    @Autowired
    public QuestionConverter(OptionConverter optionConverter) {
        this.optionConverter = optionConverter;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public QuestionDTO convertQuestionModelIntoQuestionDTO(Question question) {
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setDescription(question.getDescription());
        questionDTO.setQuestionType(question.getQuestionType());
        questionDTO.setQuizId(question.getQuiz().getId());

        List<OptionDTO> listOfPossibleOptions = new ArrayList<>();
        question.getListOfOptions().stream().forEach(option -> listOfPossibleOptions.add(OptionConverter.convertOptionModelIntoOptionDTO(option)));
        questionDTO.setListOfOptionDTOs(listOfPossibleOptions);
        return questionDTO;
    }

    public List<ResponseQuestionListDTO> convertListOfQuestionsToDTOListForResponse(List<Question> questionList) {
        List<ResponseQuestionListDTO> responseQuestionListDTOList = new ArrayList<>();
        for (Question q : questionList) {
            ResponseQuestionListDTO qDTO = convertQuestionToQuestionDTOForResponse(q);
            responseQuestionListDTOList.add(qDTO);
        }
        return responseQuestionListDTOList;
    }

    public ResponseQuestionListDTO convertQuestionToQuestionDTOForResponse(Question question) {
        ResponseQuestionListDTO responseQuestionListDTO = new ResponseQuestionListDTO();
        responseQuestionListDTO.setDescription(question.getDescription());
        responseQuestionListDTO.setQuestionType(question.getQuestionType());
        responseQuestionListDTO.setId(question.getId());
        responseQuestionListDTO.setListOfOptions(optionConverter.convertListOfQuestionsToDTOListForResponse(question.getListOfOptions()));
        return responseQuestionListDTO;
    }

    public Question toQuestionOnCreation(QuestionCreationRequestDTO questionDTO, Quiz newQuiz) {
        Question question = new Question();
        question.setDescription(questionDTO.getDescription());
        question.setQuestionType(questionDTO.getQuestionType());
        question.setQuiz(newQuiz);
        List<Option> listOfPossibleOptions = optionConverter.toListOfOptionsOnCreation(questionDTO.getListOfOptionDTOs());
        question.setListOfOptions(listOfPossibleOptions);
        return question;
    }

    public List<Question> toListOfQuestionsOnCreation(List<QuestionCreationRequestDTO> listOfQuestionDTOs, Quiz newQuiz) {
        List<Question> listOfQuestions = new ArrayList<>();
        for (QuestionCreationRequestDTO dto : listOfQuestionDTOs) {
            listOfQuestions.add(toQuestionOnCreation(dto, newQuiz));
        }
        return listOfQuestions;
    }
}
