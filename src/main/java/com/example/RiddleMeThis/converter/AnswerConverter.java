package com.example.RiddleMeThis.converter;

import com.example.RiddleMeThis.dto.AnswerDTO;
import com.example.RiddleMeThis.entities.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AnswerConverter {

    private final OptionConverter optionConverter;

    @Autowired
    public AnswerConverter(OptionConverter optionConverter) {
        this.optionConverter = optionConverter;
    }

    public Answer toAnswer(AnswerDTO dto) {
        Answer answer = new Answer();
        answer.setQuestionId(dto.getQuestionId());
        answer.setListOfOptions(optionConverter.toListOfOptions(dto.getListOfPostOptionDTOs()));
        return answer;
    }

    public List<Answer> toListOfAnswers(List<AnswerDTO> listOfAnswerDTOS) {
        List<Answer> listOfAnswers = new ArrayList<>();
        for (AnswerDTO dto : listOfAnswerDTOS) {
            listOfAnswers.add(toAnswer(dto));
        }
        return listOfAnswers;
    }

}

