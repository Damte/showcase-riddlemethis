package com.example.RiddleMeThis.converter;

import com.example.RiddleMeThis.dto.*;
import com.example.RiddleMeThis.entities.Question;
import com.example.RiddleMeThis.entities.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QuizConverter {

    private final QuestionConverter questionConverter;

    @Autowired
    public QuizConverter(QuestionConverter questionConverter) {
        this.questionConverter = questionConverter;
    }

    public QuizDTO convertQuizModelIntoQuizDTO(Quiz quiz) {
        QuizDTO quizdto = new QuizDTO();
        quizdto.setId(quiz.getId());
        quizdto.setName(quiz.getName());
        quizdto.setUserId((quiz.getUser().getId()));
        quizdto.setUuid(quiz.getQuizUUID());
        List <QuestionDTO> questionDTOList = new ArrayList <>();
        questionConverter.setQuiz(quiz);
        quiz.getListOfQuestions().stream().forEach(question -> questionDTOList.add(questionConverter.convertQuestionModelIntoQuestionDTO(question)));
        quizdto.setListOfQuestionDTOs(questionDTOList);
        return quizdto;
    }

    public ResponseQuizDTO convertQuizToQuizDTOForResponse(Quiz quiz) {
        ResponseQuizDTO responseQuizDTO = new ResponseQuizDTO();
        responseQuizDTO.setName(quiz.getName());
        responseQuizDTO.setId(quiz.getId());
        responseQuizDTO.setOwnerName(quiz.getUser().getName());

        responseQuizDTO.setListOfQuestions(questionConverter.convertListOfQuestionsToDTOListForResponse(quiz.getListOfQuestions()));
        responseQuizDTO.setListOfQuestions(questionConverter.convertListOfQuestionsToDTOListForResponse(quiz.getListOfQuestions()));
        return responseQuizDTO;
    }

    public Quiz toQuizOnCreation(QuizCreationRequestDTO quizDTO) {
        Quiz quiz = new Quiz();
        quiz.setName(quizDTO.getName());
        List<Question> questionList = questionConverter.toListOfQuestionsOnCreation(quizDTO.getListOfQuestionDTOs(), quiz);
        quiz.setListOfQuestions(questionList);
        return quiz;
    }

    public QuizCreationResponseDTO toQuizResponseDTOOnCreation(Quiz quiz) {
        return new QuizCreationResponseDTO(quiz.getId(), quiz.getName(), quiz.getQuizUUID());
    }

    public QuizStatisticsRequestDTO toQuizStatisticsRequestDTO(Quiz quiz) {
        return new QuizStatisticsRequestDTO(quiz.getId(), quiz.getName(), quiz.getListOfParticipantResponse().size());
    }



}
