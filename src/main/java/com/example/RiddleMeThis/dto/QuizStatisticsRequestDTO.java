package com.example.RiddleMeThis.dto;

public class QuizStatisticsRequestDTO {
    Long quizId;
    String quizName;
    Integer numberOfParticipantResponses;

    public QuizStatisticsRequestDTO(Long quizId, String quizName, Integer numberOfParticipantResponses) {
        this.quizId = quizId;
        this.quizName = quizName;
        this.numberOfParticipantResponses = numberOfParticipantResponses;
    }

    public QuizStatisticsRequestDTO() {
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public Integer getNumberOfParticipantResponses() {
        return numberOfParticipantResponses;
    }

    public void setNumberOfParticipantResponses(Integer numberOfParticipantResponses) {
        this.numberOfParticipantResponses = numberOfParticipantResponses;
    }
}
