package com.example.RiddleMeThis.dto;

import java.util.List;

public class PostParticipantResponseDTO {

    private String participantEmail;
    private Long quizId;
    private List<AnswerDTO> listOfAnswerDTOS;

    public PostParticipantResponseDTO(String participantEmail, Long quizId, List<AnswerDTO> listOfAnswerDTOS) {
        this.participantEmail = participantEmail;
        this.quizId = quizId;
        this.listOfAnswerDTOS = listOfAnswerDTOS;
    }

    public PostParticipantResponseDTO() {
    }

    public String getParticipantEmail() {
        return participantEmail;
    }

    public void setParticipantEmail(String participantEmail) {
        this.participantEmail = participantEmail;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public List<AnswerDTO> getListOfAnswerDTOs() {
        return listOfAnswerDTOS;
    }

    public void setListOfAnswerDTOs(List<AnswerDTO> listOfAnswerDTOS) {
        this.listOfAnswerDTOS = listOfAnswerDTOS;
    }
}
