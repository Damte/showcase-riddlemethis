package com.example.RiddleMeThis.dto;

import com.example.RiddleMeThis.entities.QuestionType;

import java.util.List;

public class QuestionCreationRequestDTO {

    private String description;
    private QuestionType questionType;
    private List<OptionCreationRequestDTO> listOfOptionDTOs;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<OptionCreationRequestDTO> getListOfOptionDTOs() {
        return listOfOptionDTOs;
    }

    public void setListOfOptionDTOs(List<OptionCreationRequestDTO> listOfOptionDTOs) {
        this.listOfOptionDTOs = listOfOptionDTOs;
    }
}

