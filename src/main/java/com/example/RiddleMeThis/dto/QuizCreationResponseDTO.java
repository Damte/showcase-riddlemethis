package com.example.RiddleMeThis.dto;

public class QuizCreationResponseDTO {

    private Long QuizId;
    private String quizName;
    private String UUID;

    public QuizCreationResponseDTO(Long quizId, String quizName, String UUID) {
        this.QuizId = quizId;
        this.quizName = quizName;
        this.UUID = UUID;
    }

    public Long getQuizId() {
        return QuizId;
    }

    public void setQuizId(Long quizId) {
        QuizId = quizId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }
}
