package com.example.RiddleMeThis.dto;

import java.util.List;

public class ResponseQuizDTO {

    private Long id;
    private String name;
    private String ownerName;
    private List<ResponseQuestionListDTO> listOfQuestions;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public List<ResponseQuestionListDTO> getListOfQuestions() {
        return listOfQuestions;
    }

    public void setListOfQuestions(List<ResponseQuestionListDTO> listOfQuestions) {
        this.listOfQuestions = listOfQuestions;
    }
}

