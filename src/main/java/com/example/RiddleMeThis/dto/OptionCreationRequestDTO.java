package com.example.RiddleMeThis.dto;

public class OptionCreationRequestDTO {

    private String description;
    private Boolean isCorrect;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }
}
