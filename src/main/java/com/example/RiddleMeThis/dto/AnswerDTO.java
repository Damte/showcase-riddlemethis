package com.example.RiddleMeThis.dto;

import java.util.List;

public class AnswerDTO {

    private Long questionId;
    private List<PostOptionDTO> listOfPostOptionDTOS;


    public AnswerDTO(Long questionId, List<PostOptionDTO> listOfPostOptionDTOS) {
        this.questionId = questionId;
        this.listOfPostOptionDTOS = listOfPostOptionDTOS;
    }

    public AnswerDTO() {
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public List<PostOptionDTO> getListOfPostOptionDTOs() {
        return listOfPostOptionDTOS;
    }

    public void setListOfPostOptionDTOs(List<PostOptionDTO> listOfPostOptionDTOS) {
        this.listOfPostOptionDTOS = listOfPostOptionDTOS;
    }
}
