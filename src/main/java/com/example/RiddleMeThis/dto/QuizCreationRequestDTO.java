package com.example.RiddleMeThis.dto;

import java.util.List;

public class QuizCreationRequestDTO {

    private String name;
    private Long userId;
    private List<QuestionCreationRequestDTO> listOfQuestionDTOs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<QuestionCreationRequestDTO> getListOfQuestionDTOs() {
        return listOfQuestionDTOs;
    }

    public void setListOfQuestionDTOs(List<QuestionCreationRequestDTO> listOfQuestionDTOs) {
        this.listOfQuestionDTOs = listOfQuestionDTOs;
    }
}
