package com.example.RiddleMeThis.dto;

import java.util.List;

public class QuizDTO {

    private Long id;
    private String name;
    private Long userId;
    private String uuid;
    private List<QuestionDTO> listOfQuestionDTOs;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<QuestionDTO> getListOfQuestionDTOs() {
        return listOfQuestionDTOs;
    }

    public void setListOfQuestionDTOs(List<QuestionDTO> listOfQuestionDTOs) {
        this.listOfQuestionDTOs = listOfQuestionDTOs;
    }
}
