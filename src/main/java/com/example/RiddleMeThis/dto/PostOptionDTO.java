package com.example.RiddleMeThis.dto;

public class PostOptionDTO {

    private Long optionId;

    public PostOptionDTO() {
    }

    public PostOptionDTO(Long id) {
    }

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }
}
