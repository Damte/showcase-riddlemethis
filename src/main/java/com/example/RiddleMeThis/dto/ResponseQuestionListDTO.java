package com.example.RiddleMeThis.dto;

import com.example.RiddleMeThis.entities.QuestionType;

import java.util.List;

public class ResponseQuestionListDTO {

    private Long id;

    private String description;

    private QuestionType questionType;

    private List<ResponseOptionListDTO> listOfOptions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<ResponseOptionListDTO> getListOfOptions() {
        return listOfOptions;
    }

    public void setListOfOptions(List<ResponseOptionListDTO> listOfOptions) {
        this.listOfOptions = listOfOptions;
    }
}
