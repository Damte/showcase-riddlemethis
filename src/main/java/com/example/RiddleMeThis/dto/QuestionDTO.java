package com.example.RiddleMeThis.dto;

import com.example.RiddleMeThis.entities.QuestionType;

import java.util.List;

public class QuestionDTO {
    private Long quizId;
    private String description;
    private QuestionType questionType;
    private List<OptionDTO> listOfOptionDTOS;


    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<OptionDTO> getListOfOptionDTOs() {
        return listOfOptionDTOS;
    }

    public void setListOfOptionDTOs(List<OptionDTO> listOfOptionDTOS) {
        this.listOfOptionDTOS = listOfOptionDTOS;
    }
}
